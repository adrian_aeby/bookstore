/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.integration.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import io.restassured.response.Response;
import java.math.BigInteger;
import java.security.SecureRandom;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.CONFLICT;
import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.NO_CONTENT;
import static javax.ws.rs.core.Response.Status.OK;
import static javax.ws.rs.core.Response.Status.PAYMENT_REQUIRED;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;
import org.books.application.dto.PurchaseOrder;
import org.books.application.dto.PurchaseOrderItem;
import org.books.application.dto.Registration;
import org.books.persistence.dto.BookInfo;
import org.books.persistence.entity.Customer;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author baechlerf
 */
public class OrderResourceIT {
    
    
    private static final String BASE_URI = "http://localhost:8080/bookstore/rest";

    @BeforeClass
    public static void init() {
        RestAssured.baseURI = BASE_URI;
    }

    @Test
    public void placeOrderPaymentRequired() {
        
        Long cutomerNr = register(false);
        
        given()
                .contentType("application/json")
                .body(generatePlaceOrderInputJSONWithOneRealBook(cutomerNr, "0596009208", true))
                .when()
                .post("orders")
                .then()
                .statusCode(PAYMENT_REQUIRED.getStatusCode())
                .extract().response();    
        
    }
    
    
    @Test
    public void placeOrderBAD_REQUEST() {
        
        Long cutomerNr = register(true);
        
        given()
                .contentType("application/json")
                .body(generatePlaceOrderInputJSONWithOneRealBook(cutomerNr, "0596009208", false))
                .when()
                .post("orders")
                .then()
                .statusCode(BAD_REQUEST.getStatusCode())
                .extract().response();    
        
    }
    
    @Test
    public void placeOrderBookNOT_FOUND() {
       
         Long cutomerNr = register(true);
        
        given()
                .contentType("application/json")
                .body(generatePlaceOrderInputJSONWithOneRealBook(cutomerNr, "123456", true))
                .when()
                .post("orders")
                .then()
                .statusCode(NOT_FOUND.getStatusCode())
                .extract().response();    
    }
    
    @Test
    public void placeOrderCustomerNOT_FOUND() {
       
        given()
                .contentType("application/json")
                .body(generatePlaceOrderInputJSON())
                .when()
                .post("orders")
                .then()
                .statusCode(NOT_FOUND.getStatusCode())
                .extract().response();    
    }

    @Test
    public void placeOrderHappyFlow() {
        
        Long cutomerNr = register(true);
        
        Response response = given()
                .contentType("application/json")
                .body(generatePlaceOrderInputJSONWithOneRealBook(cutomerNr, "0596009208", true))
                .when()
                .post("orders")
                .then()
                .statusCode(CREATED.getStatusCode())
                .extract().response();   
        ;
        
        String responseString =  response.body().asString();
        System.out.println(responseString);
    }
    
    @Test
    public void findOrderByNumberHappyFlow() {
        
        Long cutomerNr = register(true);
        
        Response response = given()
                .contentType("application/json")
                .body(generatePlaceOrderInputJSONWithOneRealBook(cutomerNr, "0596009208", true))
                .when()
                .post("orders")
                .then()
                .statusCode(CREATED.getStatusCode())
                .extract().response();   
        ;
        
        String[] result = response.body().asString().split(",");
        // Nr an Position 1
        String[] orderNr = result[0].split(":");
        
       
        Response response2 = given()
                .contentType("application/json")
                .when()
                .get("orders/"+orderNr[1])
                .then()
                .statusCode(OK.getStatusCode())
                .extract().response();   
        ;
        
        String responseString =  response2.body().asString();
        System.out.println(responseString);
    }
    
    
    @Test
    public void findOrderByNumberNOT_FOUND() {
          Response response1 = given()
                .contentType("application/json")
                .when()
                .get("orders/100000")
                .then()
                .statusCode(NOT_FOUND.getStatusCode())
                .extract().response();   
        ;
        
        String responseString =  response1.body().asString();
        System.out.println(responseString);
        
    }
    
    @Test
    public void searchOrdersOfCustomerYearMissing() {
        
        Long cutomerNr = register(true);
        
         given()
                .contentType("application/json")
                .body(generatePlaceOrderInputJSONWithOneRealBook(cutomerNr, "0596009208", true))
                .when()
                .post("orders")
                .then()
                .statusCode(CREATED.getStatusCode())
                .extract().response();   
        ;
        
         Response response2 = given()
                .contentType("application/json")
                .when()
                .get("orders?customerNr=" + cutomerNr)
                .then()
                .statusCode(BAD_REQUEST.getStatusCode())
                .extract().response();   
        ;
        
        String responseString =  response2.body().asString();
        System.out.println(responseString);
    }
    
    
    @Test
    public void searchOrdersOfCustomerCustomerNrMissing() {
        
        Long cutomerNr = register(true);
        
         given()
                .contentType("application/json")
                .body(generatePlaceOrderInputJSONWithOneRealBook(cutomerNr, "0596009208", true))
                .when()
                .post("orders")
                .then()
                .statusCode(CREATED.getStatusCode())
                .extract().response();   
        ;
        
         Response response2 = given()
                .contentType("application/json")
                .when()
                .get("orders?year=2017")
                .then()
                .statusCode(BAD_REQUEST.getStatusCode())
                .extract().response();   
        ;
        
        String responseString =  response2.body().asString();
        System.out.println(responseString);
    }
    
    @Test
    public void searchOrdersOfCustomerHappyFlow() {
        
        Long cutomerNr = register(true);
        
            given()
                .contentType("application/json")
                .body(generatePlaceOrderInputJSONWithOneRealBook(cutomerNr, "0596009208", true))
                .when()
                .post("orders")
                .then()
                .statusCode(CREATED.getStatusCode())
                .extract().response();   
        ;
        
        given()
                .contentType("application/json")
                .body(generatePlaceOrderInputJSONWithOneRealBook(cutomerNr, "0596009208", true))
                .when()
                .post("orders")
                .then()
                .statusCode(CREATED.getStatusCode())
                .extract().response();   
        ;
        
       
        Response response2 = given()
                .contentType("application/json")
                .when()
                .get("orders?customerNr=" + cutomerNr + "&year=2017")
                .then()
                .statusCode(OK.getStatusCode())
                .extract().response();   
        ;
        
        String responseString =  response2.body().asString();
        System.out.println(responseString);
        
        
    }
    
    @Test
    public void searchOrdersOfCustomerNOT_FOUND() {
        
      
        Response response2 = given()
                .contentType("application/json")
                .when()
                .get("orders?customerNr=100000&year=2017")
                .then()
                .statusCode(NOT_FOUND.getStatusCode())
                .extract().response();   
        ;
        
        String responseString =  response2.body().asString();
        System.out.println(responseString);
    }
    
    @Test
    public void cancelOrderOrderNOT_FOUND() {
         
        Response response2 = given()
                .contentType("application/json")
                .when()
                .delete("orders/100000")
                .then()
                .statusCode(NOT_FOUND.getStatusCode())
                .extract().response();   
        ;
        
        String responseString =  response2.body().asString();
        System.out.println(responseString);
    }
    
    
    @Test
    public void cancelOrderOrderHappyFlow() {
         
        Long cutomerNr = register(true);
        
        Response response = given()
                .contentType("application/json")
                .body(generatePlaceOrderInputJSONWithOneRealBook(cutomerNr, "0596009208", true))
                .when()
                .post("orders")
                .then()
                .statusCode(CREATED.getStatusCode())
                .extract().response();   
        ;
        
        String[] result = response.body().asString().split(",");
        // Nr an Position 1
        String[] orderNr = result[0].split(":");
        
        Response response2 = given()
                .contentType("application/json")
                .when()
                .delete("orders/" + orderNr[1])
                .then()
                .statusCode(NO_CONTENT.getStatusCode())
                .extract().response();   
        ;
        
        String responseString =  response2.body().asString();
        System.out.println(responseString);
    }
    
    
     @Test
    public void cancelOrderOrderNegativ() throws InterruptedException {
         
        Long cutomerNr = register(true);
        
        Response response = given()
                .contentType("application/json")
                .body(generatePlaceOrderInputJSONWithOneRealBook(cutomerNr, "0596009208", true))
                .when()
                .post("orders")
                .then()
                .statusCode(CREATED.getStatusCode())
                .extract().response();   
        ;
        
        String[] result = response.body().asString().split(",");
        // Nr an Position 1
        String[] orderNr = result[0].split(":");
        
        // warten
        Thread.sleep(12000);
          
        
        Response response2 = given()
                .contentType("application/json")
                .when()
                .delete("orders/" + orderNr[1])
                .then()
                .statusCode(FORBIDDEN.getStatusCode())
                .extract().response();   
        ;
        
        String responseString =  response2.body().asString();
        System.out.println(responseString);
    }
    
    private String generatePlaceOrderInputJSONWithOneRealBook(Long customerNr, String isbn, Boolean valid) {
       
        PurchaseOrder purchaseOrder = new PurchaseOrder();
        PurchaseOrderItem item1;
        if(valid) {
            item1 = new PurchaseOrderItem(new BookInfo(isbn, "Head First Java, 2nd Edition", new BigDecimal("44.95")), new Integer("1"));
        } else {
            item1 = new PurchaseOrderItem(new BookInfo(isbn, null, new BigDecimal("44.95")), new Integer("1"));
        }
        
        
        List<PurchaseOrderItem> items = new ArrayList<>();
        items.add(item1);
        
        purchaseOrder.setCustomerNr(customerNr);
        purchaseOrder.setItems(items);
      
        Gson gson = new GsonBuilder().create();
        
        String result = gson.toJson(purchaseOrder);
        
        System.out.println("JSON String generatePlaceOrderInputJSONWithOneRealBook: " + result);
        
        return result; 
    }
    
    
    private String generatePlaceOrderInputJSON() {
      PurchaseOrder purchaseOrder = new PurchaseOrder();
      
      PurchaseOrderItem item1 = new PurchaseOrderItem(new BookInfo("9781118407813", "Beginning Programming with Java For Dummies", new BigDecimal("44.9")), new Integer("10000000"));
      PurchaseOrderItem item2 = new PurchaseOrderItem(new BookInfo("9783734567391", "Mit dem Raspberry Pi zum eigenen Homeserver", new BigDecimal("21.9")), new Integer("10000000"));
      
      List<PurchaseOrderItem> items = new ArrayList<>();
      items.add(item1);
      items.add(item2);
      
      purchaseOrder.setCustomerNr(new Long("1"));
      purchaseOrder.setItems(items);
      
      Gson gson = new GsonBuilder().create();
      
      System.out.println("JSON String generatePlaceOrderInputJSON ist: " + gson.toJson(purchaseOrder));
      
      return gson.toJson(purchaseOrder);
    }
    
     private Long register(Boolean hasCreditcardTyp) {
        Response response = given()
                .contentType("application/json")
                .body(getRegistrationJson(hasCreditcardTyp))
                .when()
                .post("registrations")
                .then()
                .statusCode(CREATED.getStatusCode())
                .extract().response();
        ;
        System.out.println(response.body().asString());
        return Long.parseLong(response.body().asString());
    }
    
    private static final SecureRandom random = new SecureRandom();

    public String getRegistrationJson(Boolean hasCreditcardType) {
        String email = new BigInteger(130, random).toString(32);
        return getRegistrationJson(email, hasCreditcardType);
    }

    private String getRegistrationJson(String email, Boolean hasCreditcardTyp) {
        final String part1 = "{\"customer\": {  \"type\": \"customer\",  \"version\": 0,  \"address\": {    \"city\": \"Stadt\",    \"country\": \"Land\",    \"postalCode\": \"PLZ\",    \"street\": \"Nummer\"  },  \"creditCard\": {    \"expirationMonth\": 12,    \"expirationYear\": 2017,    \"number\": \"#\",    \"type\": \"VISA\"  },  \"email\": \"";
        final String part11 = "{\"customer\": {  \"type\": \"customer\",  \"version\": 0,  \"address\": {    \"city\": \"Stadt\",    \"country\": \"Land\",    \"postalCode\": \"PLZ\",    \"street\": \"Nummer\"  },  \"creditCard\": {    \"expirationMonth\": 12,    \"expirationYear\": 2017,    \"number\": \"#\",    \"type\": \"\"  },  \"email\": \"";
        
        final String part2 = "\",  \"firstName\": \"Vorname\",  \"lastName\": \"Nachname\"},\"password\":\"pwd\"}";
        
        String result;
        if (hasCreditcardTyp) {
            result = part1 + email + part2;
        } else {
            result = part11 + email + part2;
        }
        return result;
    }
}
