/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.integration.rest;

import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;
import io.restassured.response.Response;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.OK;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author baechlerf
 */
public class CatalogResourceIT {
     private static final String registrationURI = "http://localhost:8080/bookstore/rest";

    @BeforeClass
    public static void init() {
        RestAssured.baseURI = "http://localhost:8080/bookstore/rest";
    }
    
    @Test
    public void findBookByIsbnHappyFlow() {
        
        Response response = given()
                .contentType("application/json")
                .when()
                .get("books/9781118407813")
                .then()
                .statusCode(OK.getStatusCode())
                .extract().response();   
        ;
        
        String responseString =  response.body().asString();
        System.out.println(responseString);
    }
    
    @Test
    public void findBookByIsbnNegativ() {
        
        Response response = given()
                .contentType("application/json")
                .when()
                .get("books/12345678")
                .then()
                .statusCode(NOT_FOUND.getStatusCode())
                .extract().response();   
        ;
        
        String responseString =  response.body().asString();
        System.out.println(responseString);
    }
    
    @Test
    public void searchBooksByKeywordsHappyFlow() {
        
        Response response = given()
                .contentType("application/json")
                .when()
                .get("books?keywords=Java")
                .then()
                .statusCode(OK.getStatusCode())
                .extract().response();   
        ;
        
        String responseString =  response.body().asString();
        System.out.println(responseString);
    }
    
    @Test
    public void searchBooksByKeywordsNoKeywords() {
        //books?keywords={keywords}
        
         Response response = given()
                .contentType("application/json")
                .when()
                .get("books?keywordsFALSCH=Java")
                .then()
                .statusCode(BAD_REQUEST.getStatusCode())
                .extract().response();   
        ;
        
        String responseString =  response.body().asString();
        System.out.println(responseString);
    }
    
    @Test
    public void searchBooksByKeywordsEmpty() {
        //books?keywords={keywords}
        
         Response response = given()
                .contentType("application/json")
                .when()
                .get("books?keywords=")
                .then()
                .statusCode(BAD_REQUEST.getStatusCode())
                .extract().response();   
        ;
        
        String responseString =  response.body().asString();
        System.out.println(responseString);
    }
}
