/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.integration.rest;

import io.restassured.RestAssured;
import static io.restassured.RestAssured.*;
import io.restassured.response.Response;
import java.math.BigInteger;
import java.security.SecureRandom;
import javax.ws.rs.core.Response.Status;
import static javax.ws.rs.core.Response.Status.*;
import org.hamcrest.Matchers;
import org.junit.Test;
import static org.hamcrest.Matchers.*;
import org.junit.Assert;
import org.junit.BeforeClass;

/**
 *
 * @author baechlerf
 */
public class CustomerResourceIT {

    private static final String registrationURI = "http://localhost:8080/bookstore/rest";

    @BeforeClass
    public static void init() {
        RestAssured.baseURI = "http://localhost:8080/bookstore/rest";
    }

    @Test
    public void happyPath() {
        // Kunden registrieren
        Long customerNumber = register();
        // Kunden suchen mit Kundennummer
        String firstName = findCustomer(customerNumber, "firstName", "Vorname");
        // Kunden suchen mit Vornamen
        searchCustomer(firstName, true);
        // Authentisieren -> OK
        String email = authenticateCustomer(customerNumber, OK);
        // Passwort ändern
        changePassword(email, NO_CONTENT);
        // Authentisieren -> NOK
        authenticateCustomer(customerNumber, UNAUTHORIZED);
        // Kunde verändern
        updateCustomer(customerNumber, email, "irgendetwas", NO_CONTENT);
    }

    @Test
    public void updateCustomer_IncompleteData_BadRequest_400() {
        // Kunden registrieren
        Long customerNumber = register();
        // Authentisieren -> OK
        String email = authenticateCustomer(customerNumber, OK);
        // Kunde verändern: Vorname ist null
        updateCustomer(customerNumber, email, null, BAD_REQUEST);
    }

    @Test
    public void updateCustomer_NewEmailAlreadUsed_Conflict_409() {
        // Kunden registrieren
        Long customerNumber1 = register();
        // Authentisieren -> OK
        String email1 = authenticateCustomer(customerNumber1, OK);
        // Kunden registrieren
        Long customerNumber2 = register();
        // Authentisieren -> OK
        String email2 = authenticateCustomer(customerNumber2, OK);
        // Kunde verändern: Kunde 1 will Email von Kunde 2 -> geht nicht
        updateCustomer(customerNumber1, email2, "neuerVorname", CONFLICT);
    }

    @Test
    public void updateCustomer_CustomerNotFound_404() {
        updateCustomer(0L, "email", "Vorname", NOT_FOUND);
    }

    @Test
    public void searchCustomerByName_NotFound() {
        searchCustomer("NameDenEsNichtGibt", false);
    }
    
    @Test
    public void findCustomerByNumber_NotFound_404() {
        given()
                .contentType("application/json")
                .when()
                .get("customers/" + 0)
                .then()
                .statusCode(NOT_FOUND.getStatusCode())
                ;
        ;
    }

    @Test
    public void registerCustomer_EmailAlreadUsed_Conflict_409() {
        // Kunden registrieren
        Long customerNumber1 = register();
        // Authentisieren -> OK
        String email1 = authenticateCustomer(customerNumber1, OK);
        // neuer Kunde mit gleicher Email -> geht nicht
        given()
                .contentType("application/json")
                .body(getRegistrationJson(email1))
                .when()
                .post("registrations")
                .then()
                .statusCode(CONFLICT.getStatusCode())
                .extract().response();
        ;
    }
    
    @Test
    public void authenticateCustomer_NotFound() {
        authenticateCustomer(0L, NOT_FOUND);
    }
    
    @Test
    public void changePassword_NotFound() {
        changePassword("UnbekannteEmailAdresse", NOT_FOUND);
    }
    
    private void updateCustomer(Long customerNumber, String email, String newFirstName, Status status) {
        String json = getCustomerJson(email, newFirstName);
        System.out.println("updateCustomer: " + json);
        given()
                .contentType("application/json")
                .body(json)
                .when()
                .put("customers/" + customerNumber)
                .then()
                .statusCode(status.getStatusCode());
        if (status.equals(NO_CONTENT)) {
            String firstName = findCustomer(customerNumber, "firstName", newFirstName);
            Assert.assertEquals(newFirstName, firstName);
        }
    }

    private void changePassword(String email, Status status) {
        given()
                .contentType("application/json")
                .body("neuesPWD")
                .when()
                .put("registrations/" + email)
                .then()
                .statusCode(status.getStatusCode())
                .extract().response().body().asString();
        ;
    }

    private String authenticateCustomer(Long customerNumber, Status status) {
        String email;
        if (customerNumber == 0) {
            email = "unbekannteEmailAdresse";
        } else {
            email = findCustomer(customerNumber, "email", "Vorname");
        }
        String pwd = "pwd"; // ist im JSON hart kodiert

        String customerNumberRet = given()
                .contentType("application/json")
                .when()
                //                .queryParam("email", email)
                .header("password", pwd)
                .get("registrations/" + email)
                .then()
                .statusCode(status.getStatusCode())
                .extract().response().body().asString();
        ;
        if (status.equals(OK)) {
            Assert.assertEquals(customerNumber.toString(), customerNumberRet);
            return email;
        } else {
            return null;
        }
    }

    private Long register() {
        Response response = given()
                .contentType("application/json")
                .body(getRegistrationJson())
                .when()
                .post("registrations")
                .then()
                .statusCode(CREATED.getStatusCode())
                .extract().response();
        ;
        System.out.println(response.body().asString());
        return Long.parseLong(response.body().asString());
    }

    private String findCustomer(Long customerNumber, String toReturn, String firstName) {
        Response response = given()
                .contentType("application/json")
                .when()
                .get("customers/" + customerNumber)
                .then()
                .statusCode(OK.getStatusCode())
                .body("firstName", Matchers.equalTo(firstName))
                .body("lastName", Matchers.equalTo("Nachname"))
                .extract().response();
        ;
        System.out.println(response.body().asString());
        return response.body().path(toReturn);
    }

    private void searchCustomer(String name, boolean hasResult) {
        Response response = given()
                .contentType("application/json")
                .when()
                .queryParam("name", name)
                .get("customers/")
                .then()
                .statusCode(OK.getStatusCode())
                .extract().response();
        ;
        System.out.println(response.body().asString());
        Integer size = response.getBody().jsonPath().get("size()");
        if (hasResult) {
            Assert.assertThat(size, greaterThan(0));
            for (int i = 0; i < size; i++) {
                Assert.assertEquals(name, response.body().path("[" + i + "].firstName"));
            }
        } else {
            Assert.assertEquals((Integer) 0, size);
        }
    }

    private static final SecureRandom random = new SecureRandom();

    public String getRegistrationJson() {
        String email = new BigInteger(130, random).toString(32);
        return getRegistrationJson(email);
    }

    private String getRegistrationJson(String email) {
                final String part1 = "{\"customer\": {  \"type\": \"customer\",  \"version\": 0,  \"address\": {    \"city\": \"Stadt\",    \"country\": \"Land\",    \"postalCode\": \"PLZ\",    \"street\": \"Nummer\"  },  \"creditCard\": {    \"expirationMonth\": 12,    \"expirationYear\": 2017,    \"number\": \"#\",    \"type\": \"VISA\"  },  \"email\": \"";
        final String part2 = "\",  \"firstName\": \"Vorname\",  \"lastName\": \"Nachname\"},\"password\":\"pwd\"}";
        return part1 + email + part2;
    }
    
    public String getCustomerJson(String email, String firstName) {
        final String part1 = "{  \"type\": \"customer\",  \"version\": 0,  \"address\": {    \"city\": \"Stadt\",    \"country\": \"Land\",    \"postalCode\": \"PLZ\",    \"street\": \"Nummer\"  },  \"creditCard\": {    \"expirationMonth\": 12,    \"expirationYear\": 2017,    \"number\": \"#\",    \"type\": \"VISA\"  },  \"email\": \"";
        final String part2 = "\",  ";
        final String part3 = " \"lastName\": \"Nachname\", \"version\": \"1\"}";
        return part1 + email + part2 + (firstName == null ? "" : "\"firstName\": \"" + firstName + "\",") + part3;
    }


}
