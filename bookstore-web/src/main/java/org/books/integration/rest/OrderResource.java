/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.integration.rest;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;
import javax.ws.rs.core.Response;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.CONFLICT;
import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.PAYMENT_REQUIRED;
import org.books.application.dto.PurchaseOrder;
import org.books.application.dto.PurchaseOrderItem;

import org.books.application.exception.BookNotFoundException;

import org.books.application.exception.CustomerNotFoundException;
import org.books.application.exception.OrderAlreadyShippedException;
import org.books.application.exception.OrderNotFoundException;
import org.books.application.exception.PaymentFailedException;

import org.books.application.service.OrderService;
import org.books.persistence.dto.OrderInfo;
import org.books.persistence.entity.Customer;
import org.books.persistence.entity.SalesOrder;
import org.books.persistence.entity.SalesOrderItem;

/**
 *
 * @author baechlerf
 */
@RequestScoped
@Path("orders")
public class OrderResource {
    
    @EJB
    private OrderService orderService;
     
    @POST
    @Produces({APPLICATION_JSON})
    public Response placeOrder(PurchaseOrder purchaseOrder){
         
        
        // BAD REQUEST ausgelöst wenn Titel == null ist
        List<PurchaseOrderItem> items = purchaseOrder.getItems();
        
        for (PurchaseOrderItem current : items) {
            
            if (current.getBookInfo().getTitle() == null) {
                
                Response response = Response.status(BAD_REQUEST).build();
                throw new WebApplicationException("incomplete order data", response);
            }
        }
        
        
        try {
       
            SalesOrder result = orderService.placeOrder(purchaseOrder);
       
            Gson gson = new GsonBuilder().create();
             
            return Response.status(CREATED).entity(gson.toJson(result)).build();
            
        } catch (CustomerNotFoundException | BookNotFoundException ex) {
            System.out.println("exception cutomer not found");
            Response response = Response.status(NOT_FOUND).entity(ex.getMessage()).build();
            throw new WebApplicationException(ex.getMessage(), response);
           
            
        } catch (PaymentFailedException ex) {
            System.out.println("exception payment faild");
            Response response = Response.status(PAYMENT_REQUIRED).entity(ex.getMessage()).build();
            throw new WebApplicationException(ex.getMessage(), response);
        } 
    }
    
    
    @GET
    @Path("{number}")
    @Produces({APPLICATION_JSON})
    public Response findOrder(@PathParam("number") String number){
       
        try {
             SalesOrder result = orderService.findOrder(Long.valueOf(number));
             
            Gson gson = new GsonBuilder().create();
            return Response.ok(gson.toJson(result)).build();
            
        } catch (OrderNotFoundException ex) {
            System.out.println("war null");
            Response response = Response.status(NOT_FOUND).entity(ex.getMessage()).build();
            throw new WebApplicationException(ex.getMessage(), response);
        }
        
    }
    
    @GET
    @Produces({APPLICATION_JSON})
    public Response searchOrders(@QueryParam("customerNr") String number, @QueryParam("year") String year){
    
        if (number == null || number.isEmpty() || year == null || year.isEmpty()) {
            
            Response response = Response.status(BAD_REQUEST).build();
            throw new WebApplicationException(response);
        } 
        
        try {
            List<OrderInfo> result = orderService.searchOrders(Long.valueOf(number), Integer.valueOf(year));
        
            Gson gson = new GsonBuilder().create();
            return Response.ok(gson.toJson(result)).build();
            
        } catch (CustomerNotFoundException ex) {
            
            Response response = Response.status(NOT_FOUND).entity(ex.getMessage()).build();
            throw new WebApplicationException(ex.getMessage(), response);
        }
    }
    
    @DELETE
    @Path("{number}")
    public void cancelOrder(@PathParam("number") String number){
        
        try {
            orderService.cancelOrder(Long.valueOf(number));
            
        } catch (OrderNotFoundException ex) {
            
            Response response = Response.status(NOT_FOUND).entity(ex.getMessage()).build();
            throw new WebApplicationException(ex.getMessage(), response);
        
        } catch (OrderAlreadyShippedException ex) {
            
            Response response = Response.status(FORBIDDEN).entity(ex.getMessage()).build();
            throw new WebApplicationException(ex.getMessage(), response);
        }     
    }
}
