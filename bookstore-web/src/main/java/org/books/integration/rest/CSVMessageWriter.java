/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.integration.rest;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.List;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import org.books.persistence.dto.CustomerInfo;

/**
 *
 * @author Adrian
 */
@Provider
@Produces("text/csv")
public class CSVMessageWriter implements MessageBodyWriter<List>{

    @Override
    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return true;
    }

    @Override
    public long getSize(List t, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return 0;
    }

    @Override
    public void writeTo(List customerInfos, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream) throws IOException, WebApplicationException {
        PrintWriter writer = new PrintWriter(entityStream);
        for (Object o : customerInfos) {
            if (o instanceof CustomerInfo) {
                CustomerInfo c = (CustomerInfo) o;
                writer.println(c.getNumber() + "," + c.getEmail() + "," + c.getFirstName() + "," + c.getLastName());
            }
        }
        writer.flush();
        writer.close();
    }
    
}
