/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.integration.rest;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author baechlerm
 */
@ApplicationPath("rest")
public class ApplicationConfig extends Application{
    
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<>();
        classes.add(CatalogResource.class);
        classes.add(CustomerResource.class);
        classes.add(RegistrationResource.class);
        classes.add(OrderResource.class);
        classes.add(CSVMessageWriter.class);
        return classes;
    }
}
