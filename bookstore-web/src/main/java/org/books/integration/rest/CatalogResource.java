/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.integration.rest;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_XML;
import static javax.ws.rs.core.MediaType.TEXT_XML;
import javax.ws.rs.core.Response;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import org.books.application.exception.BookNotFoundException;
import org.books.application.service.CatalogService;
import org.books.integration.amazon.AmazonCatalog;
import org.books.persistence.dto.BookInfo;
import org.books.persistence.entity.Book;

/**
 *
 * @author baechlerm
 */
@RequestScoped
@Path("books")
public class CatalogResource {
    
    @EJB
    private CatalogService catalogService;
    
    // nur für Test
    
    /*
    @GET
    @Produces({TEXT_XML})
    public String test() {
        
        return "<h1>Hallo Welt</h1>";
        
    }
    */
    
    @GET
    @Path("{isbn}")
    @Produces({APPLICATION_JSON})
    //@Produces({APPLICATION_JSON,APPLICATION_XML})
    public Book findBook(@PathParam("isbn") String isbn){
        Book book = new Book();
        
        try {
            book = catalogService.findBook(isbn);
        } catch (BookNotFoundException ex) {
            
            Response response = Response.status(NOT_FOUND).entity(ex.getMessage()).build();
            throw new WebApplicationException(ex.getMessage(), response);
        }  
        
        return book;
    }
    
    @GET
    @Path("{isbn}/xml")
    @Produces({TEXT_XML})
    public Book findBookXML(@PathParam("isbn") String isbn){
        Book book = new Book();
        
        try {
            book = catalogService.findBook(isbn);
        } catch (BookNotFoundException ex) {
            Logger.getLogger(CatalogResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return book;
    }
    
    
    
    // /bookstore/rest/books?keywords={keywords}
    // URL encoded space-separated list, e.g. keywords=java+ee

    @GET
    @Produces({APPLICATION_JSON})
    public Response searchBooks(@QueryParam("keywords") String keywords) {
        
        if (keywords == null || keywords.isEmpty()) {
            Response response = Response.status(BAD_REQUEST).build();
            throw new WebApplicationException("Keywords missing", response);
        }
        
        List<BookInfo> result = new ArrayList<BookInfo>();
        System.out.println("keywords sind: " + keywords);
        result = catalogService.searchBooks(keywords);
        
        Gson gson = new GsonBuilder().create();

        return Response.ok(gson.toJson(result)).build();
    }   
    
}
