/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.integration.rest;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;
import javax.ws.rs.core.Response;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.CONFLICT;
import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.Status.NO_CONTENT;
import static javax.ws.rs.core.Response.Status.OK;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;
import org.books.application.dto.Registration;
import org.books.application.exception.CustomerAlreadyExistsException;
import org.books.application.exception.CustomerNotFoundException;
import org.books.application.exception.InvalidPasswordException;
import org.books.application.service.CustomerService;

/**
 *
 * @author Adrian
 */
@RequestScoped
@Path("registrations")
public class RegistrationResource {
    
    @EJB
    private CustomerService customerService;
    
    @POST
    @Path("")
    @Produces({TEXT_PLAIN})
    public Response registerCustomer(Registration registration){
        try {
            String customerNumber = customerService.registerCustomer(registration).toString();
            return Response.status(CREATED).entity(customerNumber).build();
            // noch schauen wegen Rollback
            // message vom BookNotFoundException
        } catch (CustomerAlreadyExistsException ex) {
            Response response = Response.status(CONFLICT).entity(ex.getMessage()).build();
            throw new WebApplicationException(ex.getMessage(), response);
        }
        
    }
    
    @GET
    @Path("{email}")
    @Produces({TEXT_PLAIN})
    public Response authenticateCustomer(@PathParam("email") String email, @HeaderParam("password") String password) {
        try {
            customerService.authenticateCustomer(email, password);
            String customerNumber = customerService.findCustomer(email).getNumber().toString();
            return Response.status(OK).entity(customerNumber).build();
        } catch (CustomerNotFoundException ex) {
            Response response = Response.status(NOT_FOUND).entity(ex.getMessage()).build();
            throw new WebApplicationException(ex.getMessage(), response);
        } catch (InvalidPasswordException ex) {
            Response response = Response.status(UNAUTHORIZED).entity(ex.getMessage()).build();
            throw new WebApplicationException(ex.getMessage(), response);
        }
    }
    
    @PUT
    @Path("{email}")
    public Response changePassword(@PathParam("email") String email, String password) {
        try {
            customerService.changePassword(email, password);
            String customerNumber = customerService.findCustomer(email).getNumber().toString();
            return Response.status(NO_CONTENT).entity(customerNumber).build();
        } catch (CustomerNotFoundException ex) {
            Response response = Response.status(NOT_FOUND).entity(ex.getMessage()).build();
            throw new WebApplicationException(ex.getMessage(), response);
        }
    }   
}
