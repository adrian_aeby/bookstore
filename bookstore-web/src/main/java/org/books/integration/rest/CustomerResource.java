/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.integration.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.GenericEntity;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_XML;
import javax.ws.rs.core.Response;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.CONFLICT;
import org.books.application.exception.CustomerAlreadyExistsException;
import org.books.application.exception.CustomerNotFoundException;
import org.books.application.service.CustomerService;
import org.books.persistence.dto.CustomerInfo;
import org.books.persistence.entity.Address;
import org.books.persistence.entity.CreditCard;
import org.books.persistence.entity.Customer;
import org.books.persistence.enumeration.CreditCardType;

/**
 *
 * @author Adrian
 */
@RequestScoped
@Path("customers")
public class CustomerResource {
    
    @EJB
    private CustomerService customerService;
    
    @GET
    @Path("{number}")
    @Produces({APPLICATION_JSON})
    public Customer findCustomer(@PathParam("number") String number){
        Customer customer;
        if (number.equals("test")) {
            Address address = new Address("Nummer", "Stadt", "PLZ", "Land");
            CreditCard creditCard = new CreditCard(CreditCardType.VISA, "#", 12, 2017);
            customer = new Customer("Vorname", "Nachname", "email", address, creditCard);
            return customer;
        }
        try {
            Long customerNumber = Long.parseLong(number);
            return customerService.findCustomer(customerNumber);
        } catch (CustomerNotFoundException ex) {
            Response response = Response.status(NOT_FOUND).entity(ex.getMessage()).build();
            throw new WebApplicationException(ex.getMessage(), response);
        } catch (NumberFormatException ex) {
            Response response = Response.status(BAD_REQUEST).entity(ex.getMessage()).build();
            throw new WebApplicationException(ex.getMessage(), response);
        }
    }
    
    @GET
    @Path("")
    @Produces({APPLICATION_JSON})
    public Response searchCustomer(@QueryParam("name") String name) {
        List<CustomerInfo> infos = customerService.searchCustomers(name);
        Gson gson = new GsonBuilder().create();
        
        return Response.ok(gson.toJson(infos)).build();
    }
    
    @GET
    @Path("")
    @Produces({"text/csv"})
    public List<CustomerInfo> searchCustomerCSV(@QueryParam("name") String name) {
        return customerService.searchCustomers(name);
    }
    
    @PUT
    @Path("{number}")
    @Produces({APPLICATION_JSON})
    public void updateCustomer(@PathParam("number") String number, Customer customer) {
        Long customerNumber = Long.parseLong(number);
        if (customer.getNumber() == null) {
            customer.setNumber(customerNumber);
        } else {
            if ( ! customerNumber.equals(customer.getNumber())) {
                String message = "Customer has wrong number: Resource=" + number + ", Customer=" + customer.getNumber();
                Response response = Response.status(BAD_REQUEST).entity(message).build();
                throw new WebApplicationException(message, response);
            }
        }
        if (incompleteData(customer)) {
            String message = "Incomplete Data: Customer=" + customer;
            Response response = Response.status(BAD_REQUEST).entity(message).build();
            throw new WebApplicationException(message, response);
        }
        try {
            customerService.updateCustomer(customer);
        } catch (CustomerNotFoundException ex) {
            Response response = Response.status(NOT_FOUND).entity(ex.getMessage()).build();
            throw new WebApplicationException(ex.getMessage(), response);
        } catch (CustomerAlreadyExistsException ex) {
            Response response = Response.status(CONFLICT).entity(ex.getMessage()).build();
            throw new WebApplicationException(ex.getMessage(), response);
        }
    }   

    private boolean incompleteData(Customer customer) {
        if (customer.getLastName() == null) {
            return true;
        }
        if (customer.getFirstName() == null) {
            return true;
        }
        if (customer.getEmail() == null) {
            return true;
        }
        if (customer.getAddress() == null) {
            return true;
        }
        if (customer.getAddress().getCity() == null) {
            return true;
        }
        if (customer.getAddress().getCountry() == null) {
            return true;
        }
        if (customer.getAddress().getPostalCode() == null) {
            return true;
        }
        if (customer.getAddress().getStreet() == null) {
            return true;
        }
        if (customer.getCreditCard() == null) {
            return true;
        }
        if (customer.getCreditCard().getExpirationMonth() == null) {
            return true;
        }
        if (customer.getCreditCard().getExpirationYear() == null) {
            return true;
        }
        if (customer.getCreditCard().getNumber() == null) {
            return true;
        }
        if (customer.getCreditCard().getType() == null) {
            return true;
        }
        return false;
    }
}
