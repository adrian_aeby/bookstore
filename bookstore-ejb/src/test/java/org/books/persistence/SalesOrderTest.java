/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.persistence;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.NoResultException;
import org.books.application.dto.PurchaseOrderItem;
import org.books.persistence.dto.OrderInfo;
import org.books.persistence.entity.Customer;
import org.books.persistence.entity.SalesOrder;
import org.books.persistence.entity.SalesOrderItem;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adrian
 */
public class SalesOrderTest extends AbstractTest{
    
    
    /*
    @Test(expected = NoResultException.class)
    public void getSalesOrderByOrderNumberTestNegativ() {
        LOGGER.info(">>>>>>>>>>>>>>>>>>> getSalesOrderByOrderNumberTestNegativ begin <<<<<<<<<<<<<<<<<<<<");
        orderRepository.getSalesOrderByOrderNumber(0l);
        LOGGER.info(">>>>>>>>>>>>>>>>>>> getSalesOrderByOrderNumberTestNegativ end   <<<<<<<<<<<<<<<<<<<<");
    }
    
    @Test(expected = NoResultException.class)
    public void getSalesOrderByOrderNumberTestWithEntityGraphNegativ() {
        LOGGER.info(">>>>>>>>>>>>>>>>>>> getSalesOrderByOrderNumberTestWithEntityGraphNegativ begin <<<<<<<<<<<<<<<<<<<<");
        orderRepository.getSalesOrderByOrderNumberWithEntityGraph(0l);
        LOGGER.info(">>>>>>>>>>>>>>>>>>> getSalesOrderByOrderNumberTestWithEntityGraphNegativ end   <<<<<<<<<<<<<<<<<<<<");
    }
    
    @Test(expected = NoResultException.class)
    public void getSalesOrderByOrderNumberWithJPQLNegativ() {
        LOGGER.info(">>>>>>>>>>>>>>>>>>> getSalesOrderByOrderNumberWithJPQLNegativ begin <<<<<<<<<<<<<<<<<<<<");
        orderRepository.getSalesOrderByOrderNumberWithJPQL(0l);
        LOGGER.info(">>>>>>>>>>>>>>>>>>> getSalesOrderByOrderNumberWithJPQLNegativ end   <<<<<<<<<<<<<<<<<<<<");
    }
    
    @Test(expected = NoResultException.class)
    public void getSalesOrderByOrderNumberWithJPQLAndEntityGraphNegativ() {
        LOGGER.info(">>>>>>>>>>>>>>>>>>> getSalesOrderByOrderNumberWithJPQLAndEntityGraphNegativ begin <<<<<<<<<<<<<<<<<<<<");
        orderRepository.getSalesOrderByOrderNumberWithJPQLAndEntityGraph(0l);
        LOGGER.info(">>>>>>>>>>>>>>>>>>> getSalesOrderByOrderNumberWithJPQLAndEntityGraphNegativ end   <<<<<<<<<<<<<<<<<<<<");
    }
    
    @Test
    public void getSalesOrderByOrderNumberTest(){
        LOGGER.info(">>>>>>>>>>>>>>>>>>> getSalesOrderByOrderNumberTest begin <<<<<<<<<<<<<<<<<<<<");
        SalesOrder order = orderRepository.getSalesOrderByOrderNumber(hs2015_1Order.getNumber());
        // assert
        assertNotNull(order);
        assertNotNull(order.getCustomer());
//        assertNotNull(order.getCustomer().getUser());
//        assertNotNull(order.getCustomer().getUser().getGroups());
//        for (GroupEntity group : order.getCustomer().getUser().getGroups()) {
//            assertNotNull(group);
//        }
        assertNotNull(order.getAddress());
        assertNotNull(order.getCreditCard());
        assertNotNull(order.getSalesOrderItems());
        for (SalesOrderItem salesOrderItem : order.getSalesOrderItems()) {
            assertNotNull(salesOrderItem);
//            assertNotNull(salesOrderItem.getBook());
        }
        assertNotNull(order.getSalesOrderItems());
        assertOrder(hs2015_1Order, order);
        LOGGER.info(">>>>>>>>>>>>>>>>>>> getSalesOrderByOrderNumberTest end   <<<<<<<<<<<<<<<<<<<<");
    }
    
    @Test
    public void getSalesOrderByOrderNumberTestWithEntityGraph(){
        LOGGER.info(">>>>>>>>>>>>>>>>>>> getSalesOrderByOrderNumberTestWithEntityGraph begin <<<<<<<<<<<<<<<<<<<<");
        SalesOrder order = orderRepository.getSalesOrderByOrderNumberWithEntityGraph(hs2015_1Order.getNumber());
        // assert
        assertNotNull(order);
        assertNotNull(order.getCustomer());
//        assertNotNull(order.getCustomer().getUser());
//        assertNotNull(order.getCustomer().getUser().getGroups());
//        for (GroupEntity group : order.getCustomer().getUser().getGroups()) {
//            assertNotNull(group);
//        }
        assertNotNull(order.getAddress());
        assertNotNull(order.getCreditCard());
        assertNotNull(order.getSalesOrderItems());
        for (SalesOrderItem salesOrderItem : order.getSalesOrderItems()) {
            assertNotNull(salesOrderItem);
            assertNotNull(salesOrderItem.getBook());
        }
        assertNotNull(order.getSalesOrderItems());
        assertOrder(hs2015_1Order, order);
        LOGGER.info(">>>>>>>>>>>>>>>>>>> getSalesOrderByOrderNumberTestWithEntityGraph end   <<<<<<<<<<<<<<<<<<<<");
    }
    
    
    @Test
    public void getSalesOrderByOrderNumberTest_JPQL(){
        LOGGER.info(">>>>>>>>>>>>>>>>>>> getSalesOrderByOrderNumberTest_JPQL begin <<<<<<<<<<<<<<<<<<<<");

        SalesOrder order = orderRepository.getSalesOrderByOrderNumberWithJPQL(hs2015_1Order.getNumber());

        LOGGER.info(order.toString());
        
        // assert
        assertNotNull(order);
        assertNotNull(order.getCustomer());
//        assertNotNull(order.getCustomer().getUser());
//        assertNotNull(order.getCustomer().getUser().getGroups());
//        for (GroupEntity group : order.getCustomer().getUser().getGroups()) {
//            assertNotNull(group);
//        }
        assertNotNull(order.getAddress());
        assertNotNull(order.getCreditCard());
        assertNotNull(order.getSalesOrderItems());
        for (SalesOrderItem salesOrderItem : order.getSalesOrderItems()) {
            assertNotNull(salesOrderItem);
            assertNotNull(salesOrderItem.getBook());
        }
        assertNotNull(order.getSalesOrderItems());
        assertOrder(hs2015_1Order, order);
        LOGGER.info(">>>>>>>>>>>>>>>>>>> getSalesOrderByOrderNumberTest_JPQL end   <<<<<<<<<<<<<<<<<<<<");
    }
    
    @Test
    public void getSalesOrderByOrderNumberTestWithEntityGraph_JPQLandEntityGraph(){
        LOGGER.info(">>>>>>>>>>>>>>>>>>> getSalesOrderByOrderNumberTestWithEntityGraph_JPQLandEntityGraph begin <<<<<<<<<<<<<<<<<<<<");
        
        SalesOrder order = orderRepository.getSalesOrderByOrderNumberWithJPQLAndEntityGraph(hs2015_1Order.getNumber());
        LOGGER.info(order.toString());
        
        // assert
        assertNotNull(order);
        assertNotNull(order.getCustomer());
//        assertNotNull(order.getCustomer().getUser());
//        assertNotNull(order.getCustomer().getUser().getGroups());
//        for (GroupEntity group : order.getCustomer().getUser().getGroups()) {
//            assertNotNull(group);
//        }
        assertNotNull(order.getAddress());
        assertNotNull(order.getCreditCard());
        assertNotNull(order.getSalesOrderItems());
        for (SalesOrderItem salesOrderItem : order.getSalesOrderItems()) {
            assertNotNull(salesOrderItem);
            assertNotNull(salesOrderItem.getBook());
        }
        assertNotNull(order.getSalesOrderItems());
        assertOrder(hs2015_1Order, order);
        LOGGER.info(">>>>>>>>>>>>>>>>>>> getSalesOrderByOrderNumberTestWithEntityGraph_JPQLandEntityGraph end   <<<<<<<<<<<<<<<<<<<<");
    }
     
    /** 
     * start test information on all orders of a particular customer and a particular year
     */
  
    
    
    
    /*
    @Test
    public void searchAllSalesOrdersByCustomerAndYear() {
        LOGGER.info(">>>>>>>>>>>>>>>>>>> searchAllSalesOrdersByCustomerAndYear begin <<<<<<<<<<<<<<<<<<<<");
        Long customerNumber = fritzMeier.getNumber();
        String year = "2015";
        List<OrderInfo> actuals = orderRepository.searchAllSalesOrdersByCustomerAndYear(customerNumber, year);
        List<OrderInfo> expecteds = getAllSalesOrders(customerNumber, year);
        LOGGER.info("actuals :"+actuals);
        LOGGER.info("expected:"+expecteds);
        Assert.assertTrue(CollectionUtils.isEqualCollection(expecteds, actuals));
        LOGGER.info(">>>>>>>>>>>>>>>>>>> searchAllSalesOrdersByCustomerAndYear end   <<<<<<<<<<<<<<<<<<<<");
    }

    @Test
    public void searchAllSalesOrderByCustromterAndYear_NoResult() {
        LOGGER.info(">>>>>>>>>>>>>>>>>>> searchAllSalesOrderByCustromterAndYear_NoResult begin <<<<<<<<<<<<<<<<<<<<");
        Long customerNumber = fritzMeier.getNumber();
        String year = "2010";
        List<OrderInfo> actuals = orderRepository.searchAllSalesOrdersByCustomerAndYear(customerNumber, year);
        List<OrderInfo> expecteds = getAllSalesOrders(customerNumber, year);
        LOGGER.info("actuals :"+actuals);
        LOGGER.info("expected:"+expecteds);
        Assert.assertTrue(CollectionUtils.isEqualCollection(expecteds, actuals));
        LOGGER.info(">>>>>>>>>>>>>>>>>>> searchAllSalesOrderByCustromterAndYear_NoResult end   <<<<<<<<<<<<<<<<<<<<");
    }
    
    private List<OrderInfo> getAllSalesOrders(Long customerNumber, String year) {
        List<OrderInfo> orders = new ArrayList<>();
        for (SalesOrder order : allOrders) {
            Long thisCustomerNumber = order.getCustomer().getNumber();
            Calendar date = new GregorianCalendar();
            date.setTime(order.getDate());
            String thisYear = String.valueOf(date.get(Calendar.YEAR));
            if (customerNumber.equals(thisCustomerNumber) && year.equals(thisYear)) {
                orders.add(new OrderInfo(order.getNumber(), order.getDate(), order.getAmount(), order.getStatus()));
            }
        }
        return orders;
    }

    /**
     * start test sum of amount and number of positions and average amount per position on all orders of all customers of a particular year group by customer
     */
    
    
    /*
    @Test
    public void searchAllSalesOrderForAllCustomersAndOneYearTest() {
        LOGGER.info(">>>>>>>>>>>>>>>>>>> searchAllSalesOrderForAllCustomersAndOneYearTest begin <<<<<<<<<<<<<<<<<<<<");
        List<OrderStatistic> actuals = orderRepository.searchAllSalesOrderForAllCustomersForOneYear("2015");
        List<OrderStatistic> expecteds = calculateOrderStatistic(orders2015);
        LOGGER.info(actuals.toString());
        LOGGER.info(expecteds.toString());
        Assert.assertTrue(CollectionUtils.isEqualCollection(expecteds, actuals));
        LOGGER.info(">>>>>>>>>>>>>>>>>>> searchAllSalesOrderForAllCustomersAndOneYearTest end   <<<<<<<<<<<<<<<<<<<<");
    }
    
    @Test
    public void searchAllSalesOrderForAllCustomersAndOneYearTest_NoResult() {
        LOGGER.info(">>>>>>>>>>>>>>>>>>> searchAllSalesOrderForAllCustomersAndOneYearTest_NoResult begin <<<<<<<<<<<<<<<<<<<<");
        List<OrderStatistic> actuals = orderRepository.searchAllSalesOrderForAllCustomersForOneYear("2010");
        List<OrderStatistic> expecteds = calculateOrderStatistic(orders2010);
        LOGGER.info(actuals.toString());
        LOGGER.info(expecteds.toString());
        Assert.assertTrue(CollectionUtils.isEqualCollection(expecteds, actuals));
        LOGGER.info(">>>>>>>>>>>>>>>>>>> searchAllSalesOrderForAllCustomersAndOneYearTest_NoResult end   <<<<<<<<<<<<<<<<<<<<");
    }
    
    /**
     * other tests
     */
    
    
    /*
    @Test
    public void setAmountTest() {
        // arrange
        SalesOrder order = new SalesOrder();
        List<SalesOrderItem> salesOrderItems = new ArrayList<>();
        salesOrderItems.add(getSalesOrderItem(new BigDecimal("19.95"), 7)); // 139.65
        salesOrderItems.add(getSalesOrderItem(new BigDecimal("20.05"), 11)); // 220.55
        salesOrderItems.add(getSalesOrderItem(new BigDecimal("21.10"), 13)); // 274.30
        order.setSalesOrderItems(salesOrderItems);
        // act
        order.setAmount();
        // assert
        assertEquals(new BigDecimal("634.50"), order.getAmount());
    }

    /**
     * helper methods
     */
    
    
    /*
    private SalesOrderItem getSalesOrderItem(BigDecimal price, Integer quantity) {
        SalesOrderItem item = new SalesOrderItem();
        item.setPrice(price);
        item.setQuantity(quantity);
        return item;
    }

    private List<OrderStatistic> calculateOrderStatistic(Set<SalesOrder> orderSet) {
        Map<Long, List<BigDecimal>> pricesMap = new HashMap<>();
        Map<Long, Customer> customerMap = new HashMap<>();
        for (SalesOrder order : orderSet) {
            Long customerNumber = order.getCustomer().getNumber();
            List<BigDecimal> prices = pricesMap.get(customerNumber);
            if (prices == null) {
                prices = new ArrayList<>();
                pricesMap.put(customerNumber, prices);
                customerMap.put(customerNumber, order.getCustomer());
            }
            for (SalesOrderItem item : order.getSalesOrderItems()) {
                prices.add(item.getPrice().multiply(new BigDecimal(item.getQuantity())));
            }
        }
        List<OrderStatistic> stat = new ArrayList<>();
        for (Map.Entry<Long, List<BigDecimal>> entry : pricesMap.entrySet()) {
            Customer customer = customerMap.get(entry.getKey());
            BigDecimal sum = new BigDecimal("0");
            long count = 0;
            for (BigDecimal price : entry.getValue()) {
                sum = sum.add(price);
                count++;
            }
            stat.add(new OrderStatistic(customer.getNumber(), customer.getFirstName(), 
                    customer.getLastName(), sum, count, sum.divide(new BigDecimal(count)).doubleValue()));
        }
        return stat;
    }

    private void assertOrder(SalesOrder expected, SalesOrder actual) {
        assertEquals(expected.getNumber(), actual.getNumber());
        assertEquals(expected.getAmount(), actual.getAmount());
        assertEquals(expected.getDate(), actual.getDate());
        assertEquals(expected.getStatus(), actual.getStatus());
        assertOrderItems(expected.getSalesOrderItems(), actual.getSalesOrderItems());
        assertEquals(expected.getCustomer(), actual.getCustomer());
    }

    private void assertOrderItems(List<SalesOrderItem> expecteds, List<SalesOrderItem> actuals) {
        assertTrue(expecteds.containsAll(actuals) && actuals.containsAll(expecteds));
    }
    
    */
}
