/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.persistence;


import java.util.HashSet;
import java.util.Set;
import javax.persistence.NoResultException;
import org.books.persistence.entity.Login;
import org.books.persistence.enumeration.UserGroup;
import org.junit.Assert;
import static org.junit.Assert.assertFalse;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author baechlerf
 */
public class UserTest extends AbstractTest {

    @Test
    public void findPositiv() {

        // arrange
        // act 
        Login result = loginRepository.find(hansSutterUser.getName());

        // assert
        Assert.assertEquals("Username falsch!", hansSutterUser.getName(), result.getName());
        Assert.assertEquals("Passwort falsch!", hansSutterUser.getPassword(), result.getPassword());
    }

    @Test
    public void findNegativ() {

        // arragne
        // act
        Login result = loginRepository.find("GIBT_ES_NICHT");

        // assert
        Assert.assertNull(result);

    }

    @Test
    public void findByEmptyParamter() {

        // arrange        
        // act
        Login result = loginRepository.find("");

        // assert
        Assert.assertNull(result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findByNullParameter() {

        // arrange
        // act
        loginRepository.find(null);

        // assert
        Assert.fail();
    }

    @Test
    public void persistAndFindPsoitiv() {

        // arrange
        String userName = "bachlerf";
        UserGroup gruppe = UserGroup.EMPLOYEE;

        em.getTransaction().begin();
        loginRepository.persist(new Login(userName, "paswort", gruppe));
        Login testUser = em.find(Login.class, userName);
        em.getTransaction().commit();
        em.clear();

        // act
        assertFalse(em.contains(testUser)); // nicht mehr im em vorhanden

        Login userFromDB = em.find(Login.class, userName);

        // assert
        Assert.assertEquals("Username falsch!", userName, userFromDB.getName());
        Assert.assertEquals("Gruppe falsch!", gruppe, userFromDB.getUserGroup());
    }
}
