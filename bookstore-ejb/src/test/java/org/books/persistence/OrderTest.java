/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.persistence;

import java.util.List;
import org.books.persistence.dto.OrderInfo;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author baechlerf
 */
public class OrderTest extends AbstractTest{
    
    @Test
    public void getAllSalesOrderByOneCustomerAndOneYearTest() {
        
        // arrange
        // act
        List<OrderInfo> result = orderRepository.search(fritzMeier, 2015);
       
        // assert
        Assert.assertEquals("Anzahl OrderInfo stimmt nicht!", 1, result.size());
    }
}
