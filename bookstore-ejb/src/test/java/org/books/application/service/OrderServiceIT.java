/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.application.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.books.application.dto.PurchaseOrder;
import org.books.application.dto.PurchaseOrderItem;
import org.books.application.dto.Registration;
import org.books.application.exception.BookAlreadyExistsException;
import org.books.application.exception.BookNotFoundException;
import org.books.application.exception.CustomerAlreadyExistsException;
import org.books.application.exception.CustomerNotFoundException;
import org.books.application.exception.OrderAlreadyShippedException;
import org.books.application.exception.OrderNotFoundException;
import org.books.application.exception.PaymentFailedException;
import static org.books.application.service.Common.createRegistration;
import static org.books.application.service.Common.getBook;
import org.books.persistence.dto.BookInfo;
import org.books.persistence.dto.OrderInfo;
import org.books.persistence.entity.Book;
import org.books.persistence.entity.CreditCard;
import org.books.persistence.entity.Customer;
import org.books.persistence.entity.SalesOrder;
import org.books.persistence.enumeration.BookBinding;
import org.books.persistence.enumeration.CreditCardType;
import org.books.persistence.enumeration.OrderStatus;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author baechlerf
 */
public class OrderServiceIT {

    private Logger LOGGER = Logger.getLogger(OrderServiceIT.class.getName());

    private static final String ORDER_SERVICE_NAME = "java:global/bookstore-ear/bookstore/OrderService";
    private static final String CUSTOMER_SERVICE_NAME = "java:global/bookstore-ear/bookstore/CustomerService";
    private static final String CATALOG_SERVICE_NAME = "java:global/bookstore-ear/bookstore/CatalogService";

    private static OrderService orderService;
    private static CustomerService customerService;
    private static CatalogService catalogService;

    private final static String DB_NAME = "bookstore";

    protected static EntityManagerFactory emf;
    protected static EntityManager em;

    private static List<BookInfo> bookInfos;
    
    @BeforeClass
    public static void lookupService() throws NamingException {
        orderService = (OrderService) new InitialContext().lookup(ORDER_SERVICE_NAME);
        customerService = (CustomerService) new InitialContext().lookup(CUSTOMER_SERVICE_NAME);
        catalogService = (CatalogService) new InitialContext().lookup(CATALOG_SERVICE_NAME);

        emf = Persistence.createEntityManagerFactory(DB_NAME);
        em = emf.createEntityManager();
        
        bookInfos = new ArrayList<>();
        BookInfo bookInfo = catalogService.searchBooks("JPA").get(0);
        bookInfos.add(bookInfo);
        bookInfo = catalogService.searchBooks("EJB").get(0);
        bookInfos.add(bookInfo);
        bookInfo = catalogService.searchBooks("web services").get(0);
        bookInfos.add(bookInfo);
        bookInfo = catalogService.searchBooks("angular").get(0);
        bookInfos.add(bookInfo);
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {

        if (em != null) {
            em.close();
        }
        if (emf != null) {
            emf.close();
        }
    }

    @Before
    public void cleanUp() {

        em.getTransaction().begin();

        int deletedFromSalesOrderItemEntity = em.createQuery("DELETE FROM SalesOrderItem").executeUpdate();
        int deletedFromSalesOrder = em.createQuery("DELETE FROM SalesOrder").executeUpdate();
//        int deletedFromBook = em.createQuery("DELETE FROM Book").executeUpdate();
        int deletedFromCustomer = em.createQuery("DELETE FROM Customer").executeUpdate();
        int deletedFromUser = em.createQuery("DELETE FROM Login").executeUpdate();

        em.getTransaction().commit();

        StringBuilder sb = new StringBuilder();

        sb.append("Deleted from:\n");
//        sb.append("BookEntity: " + deletedFromBook + "\n");
        sb.append("CustomerEntity: " + deletedFromCustomer + "\n");
        sb.append("SalesOrder: " + deletedFromSalesOrder + "\n");
        sb.append("SalesOrderItemEntity: " + deletedFromSalesOrderItemEntity + "\n");
        sb.append("UserEntity: " + deletedFromUser + "\n");

        System.out.println(sb);
    }

    @Test
    public void placeOrderPositiv() throws CustomerAlreadyExistsException, BookAlreadyExistsException, CustomerNotFoundException, BookNotFoundException, PaymentFailedException {
        // arrange
        fillDB();
        List<BookInfo> bookInfos = catalogService.searchBooks("Service-orientierte Architekturen mit Web Services");
        
        
        List<PurchaseOrderItem> purchaseOrderItems = new ArrayList<PurchaseOrderItem>();
        BigDecimal amount = new BigDecimal("0");
        for (BookInfo current : bookInfos) {
            BigDecimal anzahl = BigDecimal.ONE;
            PurchaseOrderItem item = new PurchaseOrderItem(current, anzahl.intValue());
            purchaseOrderItems.add(item);
            amount = amount.add(current.getPrice().multiply(anzahl));
        }
        Customer customer = customerService.findCustomer("frederic.baechler@test.ch");
        PurchaseOrder purchaseOrder = new PurchaseOrder(customer.getNumber(), purchaseOrderItems);

        // act
        SalesOrder result = orderService.placeOrder(purchaseOrder);

        // arrange
        assertEquals("frederic.baechler@test.ch", result.getCustomer().getEmail());
        assertEquals(amount, result.getAmount());
        assertEquals(result.getAddress(), result.getCustomer().getAddress());
        assertEquals(result.getCreditCard(), result.getCustomer().getCreditCard());
        assertEquals(bookInfos.size(), result.getSalesOrderItems().size());

    }

    @Test(expected = CustomerNotFoundException.class)
    public void placeOrderWrongUserNr() throws CustomerAlreadyExistsException, BookAlreadyExistsException, CustomerNotFoundException, BookNotFoundException, PaymentFailedException {
        fillDB();
        List<PurchaseOrderItem> purchaseOrderItems = new ArrayList<PurchaseOrderItem>();
        for (BookInfo current : bookInfos) {
            PurchaseOrderItem item = new PurchaseOrderItem(current, new Integer("1"));
            purchaseOrderItems.add(item);
        }
        PurchaseOrder purchaseOrder = new PurchaseOrder(new Long("0"), purchaseOrderItems);

        // act
        orderService.placeOrder(purchaseOrder);
    }

    @Test(expected = BookNotFoundException.class)
    public void placeOrderWrongBookInfoISBN() throws CustomerAlreadyExistsException, BookAlreadyExistsException, CustomerNotFoundException, BookNotFoundException, PaymentFailedException {
        fillDB();
        List<PurchaseOrderItem> purchaseOrderItems = new ArrayList<PurchaseOrderItem>();
        BookInfo bookInfo = new BookInfo("gibt-es-nicht", "Java lernen mit BlueJ", new BigDecimal("47.10"));
        PurchaseOrderItem item = new PurchaseOrderItem(bookInfo, new Integer("1"));
        purchaseOrderItems.add(item);

        Customer customer = customerService.findCustomer("frederic.baechler@test.ch");
        PurchaseOrder purchaseOrder = new PurchaseOrder(customer.getNumber(), purchaseOrderItems);

        // act
        orderService.placeOrder(purchaseOrder);
    }

    @Test(expected = PaymentFailedException.class)
    public void placeOrderCreditCardExpired() throws CustomerAlreadyExistsException, BookAlreadyExistsException, CustomerNotFoundException, BookNotFoundException, PaymentFailedException {
        fillDB();
        List<PurchaseOrderItem> purchaseOrderItems = new ArrayList<PurchaseOrderItem>();
        BookInfo bookInfo = new BookInfo("978-3-86894-907-0", "Java lernen mit BlueJ", new BigDecimal("37.10"));
        PurchaseOrderItem item = new PurchaseOrderItem(bookInfo, new Integer("1"));
        purchaseOrderItems.add(item);

        Customer customer = customerService.findCustomer("frederic.baechler@test.ch");
        customer.getCreditCard().setExpirationYear(2000);
        customerService.updateCustomer(customer);
        PurchaseOrder purchaseOrder = new PurchaseOrder(customer.getNumber(), purchaseOrderItems);

        // act
        orderService.placeOrder(purchaseOrder);
    }
    
    @Test
    public void serachOrdersPositiv() throws CustomerAlreadyExistsException, BookAlreadyExistsException, CustomerNotFoundException, BookNotFoundException, PaymentFailedException {
        // arrange
        fillDB();
        List<PurchaseOrderItem> purchaseOrderItems = new ArrayList<>();
        for (BookInfo current : bookInfos) {
            PurchaseOrderItem item = new PurchaseOrderItem(current, new Integer("1"));
            purchaseOrderItems.add(item);
        }
        Customer customer = customerService.findCustomer("frederic.baechler@test.ch");
        PurchaseOrder purchaseOrder = new PurchaseOrder(customer.getNumber(), purchaseOrderItems);
        SalesOrder order1 = orderService.placeOrder(purchaseOrder);
        
        List<BookInfo> bookInfos2 = catalogService.searchBooks("lernen");
        List<PurchaseOrderItem> purchaseOrderItems2 = new ArrayList<>();
        for (BookInfo current : bookInfos2) {
            PurchaseOrderItem item2 = new PurchaseOrderItem(current, new Integer("1"));
            purchaseOrderItems2.add(item2);
        }
        
        PurchaseOrder purchaseOrder2 = new PurchaseOrder(customer.getNumber(), purchaseOrderItems2);
        SalesOrder order2 = orderService.placeOrder(purchaseOrder2);
        
        Calendar cal = new GregorianCalendar();
        int year = (cal.get(Calendar.YEAR)); 
        //act
        List<OrderInfo> result = orderService.searchOrders(customer.getNumber(), year);
        
        //assert
        assertEquals(2,result.size());  
        assertEquals(order1.getNumber(),result.get(0).getNumber());  
        assertEquals(order2.getNumber(),result.get(1).getNumber());  
    }
    
    @Test
    public void serachOrdersPositiv2Customer() throws CustomerAlreadyExistsException, BookAlreadyExistsException, CustomerNotFoundException, BookNotFoundException, PaymentFailedException {
        // arrange
        fillDB();
        List<PurchaseOrderItem> purchaseOrderItems = new ArrayList<>();
        for (BookInfo current : bookInfos) {
            PurchaseOrderItem item = new PurchaseOrderItem(current, new Integer("1"));
            purchaseOrderItems.add(item);
        }
        Customer customer = customerService.findCustomer("frederic.baechler@test.ch");
        PurchaseOrder purchaseOrder = new PurchaseOrder(customer.getNumber(), purchaseOrderItems);
        SalesOrder order1 = orderService.placeOrder(purchaseOrder);
        
        List<BookInfo> bookInfos2 = catalogService.searchBooks("lernen");
        List<PurchaseOrderItem> purchaseOrderItems2 = new ArrayList<>();
        for (BookInfo current : bookInfos2) {
            PurchaseOrderItem item2 = new PurchaseOrderItem(current, new Integer("1"));
            purchaseOrderItems2.add(item2);
        }
        
        Customer customer2 = customerService.findCustomer("adrian.aeby@test.ch");
        PurchaseOrder purchaseOrder2 = new PurchaseOrder(customer2.getNumber(), purchaseOrderItems2);
        SalesOrder order2 = orderService.placeOrder(purchaseOrder2);
        
        Calendar cal = new GregorianCalendar();
        int year = (cal.get(Calendar.YEAR)); 
        //act
        List<OrderInfo> result = orderService.searchOrders(customer.getNumber(), year);
        
        //assert
        assertEquals(1,result.size());  
        assertEquals(order1.getNumber(),result.get(0).getNumber());  
    }
    
    @Test
    public void searchOrders_ReturnEmptyList() throws Exception{
        //arrange
        fillDB();
        Customer customer = customerService.findCustomer("adrian.aeby@test.ch");
        
        //act
        List<OrderInfo> result = orderService.searchOrders(customer.getNumber(), 2016);
        
        //assert
        assertEquals(0, result.size());
    }
    
    @Test(expected = CustomerNotFoundException.class)
    public void searchOrder_CustomerNotFound() throws Exception{
        //arrange
        fillDB();
        
        //act
        orderService.searchOrders(new Long("0"), 2016);
    }
    
    @Test
    public void checkOrderStatus() throws Exception {
        fillDB();
        List<PurchaseOrderItem> purchaseOrderItems = new ArrayList<PurchaseOrderItem>();
        for (BookInfo current : bookInfos) {
            PurchaseOrderItem item = new PurchaseOrderItem(current, new Integer("1"));
            purchaseOrderItems.add(item);
        }
        Customer customer = customerService.findCustomer("frederic.baechler@test.ch");
        PurchaseOrder purchaseOrder = new PurchaseOrder(customer.getNumber(), purchaseOrderItems);

        SalesOrder order = orderService.placeOrder(purchaseOrder);

        assertStatus(order.getNumber(), OrderStatus.ACCEPTED);
        Thread.sleep(1000);
        assertStatus(order.getNumber(), OrderStatus.PROCESSING);
        Thread.sleep(10000);
        assertStatus(order.getNumber(), OrderStatus.SHIPPED);

    }
    
    @Test
    public void cancleOrderPositiv() throws Exception {
        //arrang
        fillDB();
        List<PurchaseOrderItem> purchaseOrderItems = new ArrayList<PurchaseOrderItem>();
        for (BookInfo current : bookInfos) {
            PurchaseOrderItem item = new PurchaseOrderItem(current, new Integer("1"));
            purchaseOrderItems.add(item);
        }
        Customer customer = customerService.findCustomer("frederic.baechler@test.ch");
        PurchaseOrder purchaseOrder = new PurchaseOrder(customer.getNumber(), purchaseOrderItems);
        SalesOrder salesOrder = orderService.placeOrder(purchaseOrder);
         
        Long orderNr = salesOrder.getNumber();
        
        Thread.sleep(1000); // warten dass der Status auf PROCESSING wechselt
        SalesOrder before = orderService.findOrder(orderNr);
        OrderStatus statusBefore = before.getStatus();
        
        //act
        orderService.cancelOrder(orderNr);
        
        //arrange
        SalesOrder after = orderService.findOrder(salesOrder.getNumber());
        OrderStatus statusAfter = after.getStatus();
        
        assertEquals(statusBefore, OrderStatus.PROCESSING);
        assertEquals(statusAfter, OrderStatus.CANCELED);   
    }
    
    @Test(expected = OrderAlreadyShippedException.class)
    public void cancleOrder_OrderAlreadyShipped() throws Exception {
        //arrang
        fillDB();
        List<PurchaseOrderItem> purchaseOrderItems = new ArrayList<PurchaseOrderItem>();
        for (BookInfo current : bookInfos) {
            PurchaseOrderItem item = new PurchaseOrderItem(current, new Integer("1"));
            purchaseOrderItems.add(item);
        }
        Customer customer = customerService.findCustomer("frederic.baechler@test.ch");
        PurchaseOrder purchaseOrder = new PurchaseOrder(customer.getNumber(), purchaseOrderItems);
        SalesOrder salesOrder = orderService.placeOrder(purchaseOrder);
         
        Long orderNr = salesOrder.getNumber();
        
        //act
        Thread.sleep(12000);
        orderService.cancelOrder(orderNr);
    }
    
    @Test(expected = OrderNotFoundException.class)
    public void cancleOrder_OrderNotFound() throws Exception {
        //arrang
        fillDB();
        List<PurchaseOrderItem> purchaseOrderItems = new ArrayList<PurchaseOrderItem>();
        for (BookInfo current : bookInfos) {
            PurchaseOrderItem item = new PurchaseOrderItem(current, new Integer("1"));
            purchaseOrderItems.add(item);
        }
        Customer customer = customerService.findCustomer("frederic.baechler@test.ch");
        PurchaseOrder purchaseOrder = new PurchaseOrder(customer.getNumber(), purchaseOrderItems);
        SalesOrder salesOrder = orderService.placeOrder(purchaseOrder);
        
        //act
        orderService.cancelOrder(new Long("0"));
    }
    
    @Test
    public void findOrderPositiv() throws Exception {
        //arrang
        fillDB();
        List<PurchaseOrderItem> purchaseOrderItems = new ArrayList<PurchaseOrderItem>();
        for (BookInfo current : bookInfos) {
            PurchaseOrderItem item = new PurchaseOrderItem(current, new Integer("1"));
            purchaseOrderItems.add(item);
        }
        Customer customer = customerService.findCustomer("frederic.baechler@test.ch");
        PurchaseOrder purchaseOrder = new PurchaseOrder(customer.getNumber(), purchaseOrderItems);
        SalesOrder salesOrder = orderService.placeOrder(purchaseOrder);
        
        //act
        SalesOrder result = orderService.findOrder(salesOrder.getNumber());
        //assert
        Assert.assertEquals("frederic.baechler@test.ch",result.getCustomer().getEmail());
        Assert.assertEquals(salesOrder.getNumber(),result.getNumber());
    }
    
    @Test(expected = OrderNotFoundException.class)
    public void findOrder_OrderNotFound() throws Exception {
        
        //act
        orderService.findOrder(new Long("0"));
    }
    
    @Test
    public void checkCreditCardVisa() throws Exception {
        CreditCard creditCard = new CreditCard(CreditCardType.VISA, "123", 1, 2020);
        OrderServiceBean.checkCreditCard(creditCard);
    }

    @Test
    public void checkCreditCardMastercard() throws Exception {
        CreditCard creditCard = new CreditCard(CreditCardType.MASTERCARD, "123", 1, 2020);
        OrderServiceBean.checkCreditCard(creditCard);
    }

    @Test (expected = PaymentFailedException.class)
    public void checkCreditCardNoType() throws Exception {
        CreditCard creditCard = new CreditCard(null, "123", 1, 2020);
        OrderServiceBean.checkCreditCard(creditCard);
    }

    @Test (expected = PaymentFailedException.class)
    public void checkCreditYearEqualMonthExpired() throws Exception {
        CreditCard creditCard = new CreditCard(CreditCardType.MASTERCARD, "123", 11, 2016);
        OrderServiceBean.checkCreditCard(creditCard);
    }

    @Test
    public void checkCreditYearEqualMonthEqaul() throws Exception {
        CreditCard creditCard = new CreditCard(CreditCardType.MASTERCARD, "123", 2, 2017);
        OrderServiceBean.checkCreditCard(creditCard);
    }

    @Test
    public void checkCreditYearGreather() throws Exception {
        CreditCard creditCard = new CreditCard(CreditCardType.MASTERCARD, "123", 1, 2018);
        OrderServiceBean.checkCreditCard(creditCard);
    }

    @Test (expected = PaymentFailedException.class)
    public void checkCreditYearExpired() throws Exception {
        CreditCard creditCard = new CreditCard(CreditCardType.MASTERCARD, "123", 12, 2015);
        OrderServiceBean.checkCreditCard(creditCard);
    }

    private void fillDB() throws CustomerAlreadyExistsException, BookAlreadyExistsException {

        Registration registration1 = createRegistration("Passwort", "frederic.baechler@test.ch", "Frédéric", "Bächler");
        customerService.registerCustomer(registration1);
        Registration registration2 = createRegistration("pw", "adrian.aeby@test.ch", "Adrian", "Aeby");
        customerService.registerCustomer(registration2);

//        Book book1 = getBook("978-3-86894-907-0", "David Barnes", BookBinding.PAPERBACK, 672, new BigDecimal("47.10"), 2013, "Pearson", "Java lernen mit BlueJ");
//        catalogService.addBook(book1);
//        Book book2 = getBook("978-3-95845-055-4", "Wolfgang Höfer", BookBinding.PAPERBACK, 576, new BigDecimal("45.90"), 2016, "MITP Verlags GmbH & Co. KG", "Raspberry Pi programmieren mit Java");
//        catalogService.addBook(book2);
//        Book book3 = getBook("978-3-8362-4119-9", "Christian Ullenboom", BookBinding.PAPERBACK, 1312, new BigDecimal("67"), 2016, "Rheinwerk Verlag", "Java ist auch eine Insel");
//        catalogService.addBook(book3);
    }

    private void assertStatus(Long number, OrderStatus orderStatus) throws OrderNotFoundException {
        SalesOrder order = orderService.findOrder(number);
        Assert.assertEquals(orderStatus, order.getStatus());
    }
}
