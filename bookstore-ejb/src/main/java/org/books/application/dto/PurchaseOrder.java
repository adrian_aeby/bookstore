/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.application.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Adrian
 */
public class PurchaseOrder implements Serializable {
    
    private Long customerNr;
    
    private List<PurchaseOrderItem> items;

    public PurchaseOrder() {
    }

    public PurchaseOrder(Long customerNr, List<PurchaseOrderItem> items) {
        this.customerNr = customerNr;
        this.items = items;
    }

    public Long getCustomerNr() {
        return customerNr;
    }

    public void setCustomerNr(Long customerNr) {
        this.customerNr = customerNr;
    }

    public List<PurchaseOrderItem> getItems() {
        return items;
    }

    public void setItems(List<PurchaseOrderItem> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "PurchaseOrder{" + "customerNr=" + customerNr + ", items=" + items + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.customerNr);
        hash = 53 * hash + Objects.hashCode(this.items);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PurchaseOrder other = (PurchaseOrder) obj;
        if (!Objects.equals(this.customerNr, other.customerNr)) {
            return false;
        }
        if (!Objects.equals(this.items, other.items)) {
            return false;
        }
        return true;
    }   
}
