/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.application.dto;

import java.io.Serializable;
import java.util.Objects;
import org.books.persistence.dto.BookInfo;

/**
 *
 * @author Adrian
 */
public class PurchaseOrderItem implements Serializable {
    
    private BookInfo bookInfo;
    private Integer quantity;

    public PurchaseOrderItem() {
    }

    public PurchaseOrderItem(BookInfo bookInfo, Integer quantity) {
        this.bookInfo = bookInfo;
        this.quantity = quantity;
    }

    public BookInfo getBookInfo() {
        return bookInfo;
    }

    public void setBookInfo(BookInfo bookInfo) {
        this.bookInfo = bookInfo;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "PurchaseOrderItem{" + "bookInfo=" + bookInfo + ", quantity=" + quantity + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.bookInfo);
        hash = 71 * hash + Objects.hashCode(this.quantity);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PurchaseOrderItem other = (PurchaseOrderItem) obj;
        if (!Objects.equals(this.bookInfo, other.bookInfo)) {
            return false;
        }
        if (!Objects.equals(this.quantity, other.quantity)) {
            return false;
        }
        return true;
    }
}
