/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.application.interceptor;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import org.books.application.service.MailBean;

/**
 *
 * @author Adrian
 */
public class LogInterceptor {
    private Logger logger = Logger.getLogger(LogInterceptor.class.getName());
    @AroundInvoke
    public Object log(InvocationContext ic) throws Exception {
        String methodName = ic.getMethod().getDeclaringClass().getName() 
                + "." + ic.getMethod().getName();
        StringBuilder parameters = new StringBuilder();
        String delimiter = "";
        if (ic.getParameters() != null) {
            for (Object object : ic.getParameters()) {
            parameters.append(delimiter).append(object);
            delimiter = ", ";
            }
        }
        logger.log(Level.INFO, "ENTERING method: {0} parameter = {1}", 
                new String[]{methodName, parameters.toString()});
        Object result = ic.proceed();
        logger.log(Level.INFO, "EXITING method: {0} returns {1}", 
                new String[]{methodName, result == null ? "null" : result.toString()});
        return result;
    }
}