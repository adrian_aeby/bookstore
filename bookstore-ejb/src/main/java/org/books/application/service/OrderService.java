/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.application.service;

import java.util.List;
import javax.ejb.Remote;
import org.books.application.dto.PurchaseOrder;
import org.books.application.exception.BookNotFoundException;
import org.books.application.exception.CustomerNotFoundException;
import org.books.application.exception.OrderAlreadyShippedException;
import org.books.application.exception.OrderNotFoundException;
import org.books.application.exception.PaymentFailedException;
import org.books.persistence.dto.OrderInfo;
import org.books.persistence.entity.SalesOrder;

/**
 *
 * @author Adrian
 */
@Remote
public interface OrderService {
    /**
     * Cancels an order.
     * @param orderNr the number of the order
     * @throws OrderNotFoundException if no order with the specified number exists
     * @throws OrderAlreadyShippedException  if the order has already been shipped
     */
    void cancelOrder(Long orderNr) throws OrderNotFoundException, OrderAlreadyShippedException;

    /**
     * Finds an order by number.
     * @param orderNr the number of the order
     * @return the data of the found order
     * @throws OrderNotFoundException if no order with the specified number exists
     */
    SalesOrder findOrder(Long orderNr) throws OrderNotFoundException;
    
    /**
     * Places an order on the bookstore.
     * @param purchaseOrder the data of the order to be placed
     * @return the data of the placed order
     * @throws CustomerNotFoundException if no customer with the specified number exists
     * @throws BookNotFoundException if an order item references a non-existent book
     * @throws PaymentFailedException if a payment error occurred
     */
    SalesOrder placeOrder(PurchaseOrder purchaseOrder) throws CustomerNotFoundException, BookNotFoundException, PaymentFailedException;

    /**
     * Searches for orders by customer and year.
     * @param customerNr the number of the customer
     * @param year the year of the orders
     * @return a list of matching orders (may be empty)
     * @throws CustomerNotFoundException if no customer with the specified email address exists
     */
    List<OrderInfo> searchOrders(Long customerNr, Integer year) throws CustomerNotFoundException;
}
