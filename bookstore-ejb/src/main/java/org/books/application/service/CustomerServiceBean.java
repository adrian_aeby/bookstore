/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.application.service;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.books.application.dto.Registration;
import org.books.application.exception.CustomerAlreadyExistsException;
import org.books.application.exception.CustomerNotFoundException;
import org.books.application.exception.InvalidPasswordException;
import org.books.persistence.dto.CustomerInfo;
import org.books.persistence.entity.Customer;
import org.books.persistence.entity.Login;
import org.books.persistence.enumeration.UserGroup;
import org.books.persistence.repository.CustomerRepository;
import org.books.persistence.repository.LoginRepository;

/**
 *
 * @author Adrian
 */
@Stateless(name = "CustomerService")
public class CustomerServiceBean implements CustomerService {

    @EJB
    CustomerRepository customerRepository;
    
    @EJB
    LoginRepository loginRepository;
    
    /**
     * Authenticates a customer.
     * @param email the email address of the customer
     * @param password the password of the customer
     * @throws CustomerNotFoundException if no customer with the specified email address exists
     * @throws InvalidPasswordException if the password is invalid
     */
    @Override
    public void authenticateCustomer(String email, String password) throws CustomerNotFoundException, InvalidPasswordException{
        Login login = findLogin(email);
        
        checkPassword(login, password);
    }

    /**
     * Changes the password of a customer.
     * @param email the email address of the customer
     * @param password the password of the customer
     * @throws CustomerNotFoundException if no customer with the specified email address exists
     */
    @Override
    public void changePassword(String email, String password) throws CustomerNotFoundException{
        Login login = findLogin(email);
        
//        checkPassword(login, password);

        login.setPassword(password);
        loginRepository.update(login);
    }

    /**
     * Finds a customer by number.
     * @param customerNr the number of the customer
     * @return the data of the found customer
     * @throws CustomerNotFoundException if no customer with the specified email address exists
     */
    @Override
    public Customer findCustomer(Long customerNr) throws CustomerNotFoundException{
        Customer customer = customerRepository.find(customerNr);
        
        if (customer == null) {
            throw new CustomerNotFoundException();
        } else {
            return customer;
        }
    }

    /**
     * Finds a customer by email address.
     * @param email the email address of the customer
     * @return the data of the found customer
     * @throws CustomerNotFoundException if no customer with the specified email address exists
     */
    @Override
    public Customer findCustomer(String email) throws CustomerNotFoundException{
        Customer customer = customerRepository.find(email);
        
        if (customer == null) {
            throw new CustomerNotFoundException();
        } else {
            return customer;
        }
    }

    /**
     * Registers a customer with the bookstore.
     * The email address and password will be used to authenticate the customer.
     * @param registration the registration data of the customer
     * @return the number of the registered customers
     * @throws CustomerAlreadyExistsException if another customer with the same email address alread exists
     */
    @Override
    public Long registerCustomer(Registration registration) throws CustomerAlreadyExistsException{
        String email = registration.getCustomer().getEmail();
        checkFreeUsername(email);
        
        String password = registration.getPassword();
        
        Login login = new Login(email, password, UserGroup.CUSTOMER);
                
        loginRepository.persist(login);
        customerRepository.persist(registration.getCustomer());
        
        return registration.getCustomer().getNumber();
    }

    /**
     * Searches for customers by name.
     * A customer is included in the result list if the specified name is part of the customer's first or last name.
     * @param name the name to search for
     * @return a list of matching customers (may be empty)
     */
    @Override
    public List<CustomerInfo> searchCustomers(String name){
        return customerRepository.search("%"+name+"%");
    }

    /**
     * Updates the data of a customer.
     * If the email address is to be changed, the new email address is used for authentication.
     * @param customer the data of the customer to be updated (the number must not be null)
     * @throws CustomerNotFoundException if no customer with the specified email address exists
     * @throws CustomerAlreadyExistsException if another customer with the same email address alread exists
     */
    @Override
    public void updateCustomer(Customer customer) throws CustomerNotFoundException, CustomerAlreadyExistsException {
        Customer oldCustomer = findCustomer(customer.getNumber());
        String oldEmail = oldCustomer.getEmail();
        String newEmail = customer.getEmail();
        
        // has the email (username) changed?
        if ( ! oldEmail.equalsIgnoreCase(newEmail)) {
            // check new email (username)
            checkFreeUsername(newEmail);
            
            Login oldLogin = findLogin(oldEmail);
            Login newLogin = new Login(newEmail, oldLogin.getPassword(), oldLogin.getUserGroup());
            loginRepository.persist(newLogin);
            loginRepository.delete(Login.class, oldLogin.getName());
        }
        
        customer.setNumber(oldCustomer.getNumber());
        customerRepository.update(customer);
    }
    
    private Login findLogin(String email) throws CustomerNotFoundException {
        Login login = loginRepository.find(email);
        if (login == null) {
            throw new CustomerNotFoundException();
        }
        return login;
    }
    
    private void checkFreeUsername(String email) throws CustomerAlreadyExistsException {
        Login login = loginRepository.find(email);
        
        if (login != null) {
            throw new CustomerAlreadyExistsException(email + " bereits vergeben!");
        }
    }

    private void checkPassword(Login login, String password) throws InvalidPasswordException {
        if ( ! login.getPassword().equals(password)) {
            throw new InvalidPasswordException();
        }
    }
}
