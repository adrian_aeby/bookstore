/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.persistence.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import org.books.persistence.enumeration.CreditCardType;

/**
 *
 * @author Adrian
 */
@Embeddable
public class CreditCard implements Serializable {
    
    @Enumerated(EnumType.STRING)
    private CreditCardType type; 
    @Column(name="Credit_Card_Number")
    private String number;
    private Integer expirationMonth;
    private Integer expirationYear;

    public CreditCard() {
    }

    public CreditCard(CreditCardType type, String number, Integer expirationMonth, Integer expirationYear) {
        this.type = type;
        this.number = number;
        this.expirationMonth = expirationMonth;
        this.expirationYear = expirationYear;
    }

    public CreditCardType getType() {
        return type;
    }

    public void setType(CreditCardType type) {
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getExpirationMonth() {
        return expirationMonth;
    }

    public void setExpirationMonth(Integer expirationMonth) {
        this.expirationMonth = expirationMonth;
    }

    public Integer getExpirationYear() {
        return expirationYear;
    }

    public void setExpirationYear(Integer expirationYear) {
        this.expirationYear = expirationYear;
    }

    @Override
    public String toString() {
        return "CreditCard{" + "type=" + type + ", number=" + number + ", expirationMonth=" + expirationMonth + ", expirationYear=" + expirationYear + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + Objects.hashCode(this.type);
        hash = 41 * hash + Objects.hashCode(this.number);
        hash = 41 * hash + Objects.hashCode(this.expirationMonth);
        hash = 41 * hash + Objects.hashCode(this.expirationYear);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CreditCard other = (CreditCard) obj;
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        if (!Objects.equals(this.number, other.number)) {
            return false;
        }
        if (!Objects.equals(this.expirationMonth, other.expirationMonth)) {
            return false;
        }
        if (!Objects.equals(this.expirationYear, other.expirationYear)) {
            return false;
        }
        return true;
    }     
}
