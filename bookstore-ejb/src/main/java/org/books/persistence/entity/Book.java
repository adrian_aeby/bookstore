/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.persistence.entity;

import java.math.BigDecimal;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.books.persistence.enumeration.BookBinding;

/**
 *
 * @author Adrian
 */
@Entity
@Table(name = "BOOK")
public class Book extends BaseEntity {

    @Id
    private String isbn;  // Items/item/itemAttributes/ISBN
    private String title; // Items/item/itemAttributes/title
    private String authors;
    private String publisher;
//    @Column(columnDefinition = "NUMBER(10)")
    private Integer publicationYear;
    
    @Enumerated(EnumType.STRING)
    private BookBinding binding; 
    
//    @Column(columnDefinition = "NUMBER(10)")
    private Integer numberOfPages;
    @Column(columnDefinition = "DECIMAL(7,2)")
    private BigDecimal price;
    @Transient
    private boolean complete;

    public Book() {
    }

    public Book(String isbn, String title, String authors, String publisher, Integer publicationYear, BookBinding binding, Integer numberOfPages, BigDecimal price) {
        setIsbn(isbn);
        this.title = title;
        this.authors = authors;
        this.publisher = publisher;
        this.publicationYear = publicationYear;
        this.binding = binding;
        this.numberOfPages = numberOfPages;
        this.price = price;
    }

    
    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        if (isbn != null) {
            isbn = isbn.toUpperCase();
        }
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Integer getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(Integer publicationYear) {
        this.publicationYear = publicationYear;
    }

    public BookBinding getBinding() {
        return binding;
    }

    public void setBinding(BookBinding binding) {
        this.binding = binding;
    }

    public Integer getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(Integer numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }
    
    @Override
    public String toString() {
        return "Book{" + "isbn=" + isbn + ", title=" + title + ", authors=" + authors + ", publisher=" + publisher + ", publicationYear=" + publicationYear + ", binding=" + binding + ", numberOfPages=" + numberOfPages + ", price=" + price + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.isbn);
        hash = 97 * hash + Objects.hashCode(this.title);
        hash = 97 * hash + Objects.hashCode(this.authors);
        hash = 97 * hash + Objects.hashCode(this.publisher);
        hash = 97 * hash + Objects.hashCode(this.publicationYear);
        hash = 97 * hash + Objects.hashCode(this.binding);
        hash = 97 * hash + Objects.hashCode(this.numberOfPages);
        hash = 97 * hash + Objects.hashCode(this.price);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Book other = (Book) obj;
        if (!Objects.equals(this.isbn, other.isbn)) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Objects.equals(this.authors, other.authors)) {
            return false;
        }
        if (!Objects.equals(this.publisher, other.publisher)) {
            return false;
        }
        if (!Objects.equals(this.binding, other.binding)) {
            return false;
        }
        if (!Objects.equals(this.publicationYear, other.publicationYear)) {
            return false;
        }
        if (!Objects.equals(this.numberOfPages, other.numberOfPages)) {
            return false;
        }
        if (!Objects.equals(this.price, other.price)) {
            return false;
        }
        return true;
    }   
}
