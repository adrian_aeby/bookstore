/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.persistence.entity;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import org.books.persistence.enumeration.UserGroup;

/**
 *
 * @author Adrian
 */
@Entity
@Table(name = "LOGIN")
public class Login extends BaseEntity {
    
    @Id
    private String userName;
    private String password;
    
    @Column(name="USERGROUP")
    @Enumerated(EnumType.STRING)
    private UserGroup group;

    public Login() {
    }

    public Login(String userName, String password, UserGroup group) {
        this.userName = userName;
        this.password = password;
        this.group = group;
    }

    public String getName() {
        return userName;
    }

    public void setName(String name) {
        this.userName = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserGroup getUserGroup() {
        return group;
    }

    public void setUserGroup(UserGroup groups) {
        this.group = groups;
    }

    @Override
    public String toString() {
        return "User{" + "name=" + userName + ", password=" + password + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(this.userName);
        hash = 17 * hash + Objects.hashCode(this.password);
        hash = 17 * hash + Objects.hashCode(this.group);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Login other = (Login) obj;
        if (!Objects.equals(this.userName, other.userName)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        if (!Objects.equals(this.group, other.group)) {
            return false;
        }
        return true;
    }   
}