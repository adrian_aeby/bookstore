/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.persistence.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author baechlerf
 */
public class BookInfo implements Serializable {
    
    private String isbn;
    private String title;
    private BigDecimal price;
    
    public final static String BOOKINFO_WITH_ISBN = "SELECT NEW " + BookInfo.class.getName() + "(b.isbn, b.title, b.price) FROM Book b where b.isbn = :isbn";

    public BookInfo() { 
    }
    
    public BookInfo(String isbn, String title, BigDecimal price) {
        this.isbn = isbn;
        this.title = title;
        this.price = price;
    }
    
    public String getIsbn() {
        return this.isbn;
    }
    
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
        
    public BigDecimal getPrice() {
        return this.price;
    }
    
    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    @Override
    public String toString() {
        return "BookInfo{" + "isbn=" + isbn + ", title=" + title + ", price=" + price.toString() + '}';
    }
}
