/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.persistence.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Objects;
import org.books.persistence.enumeration.OrderStatus;

/**
 *
 * @author Adrian
 */
public class OrderInfo implements Serializable {
    
    public final static String GET_ALL_SALESORDER_BY_CUSTOMER_AND_YEAR = 
            "SELECT NEW " + OrderInfo.class.getName() + "("
          + "o.number, o.date, o.amount, o.status) "
          + "FROM SalesOrder o JOIN o.customer c "
          + "WHERE c.number = :customerNumber "
          + "  AND o.date between :from and :to";
    
    private Long number;
    private Date date;
    private BigDecimal amount;
    private OrderStatus status; 

    public OrderInfo() {        
    }
    
    public OrderInfo(Long number, Date date, BigDecimal amount, OrderStatus status) {
        this.number = number;
        this.date = date;
        this.amount = amount;
        this.status = status;
    }

    public Long getNumber() {
        return number;
    }

    public Date getDate() {
        return date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public OrderStatus getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "OrderInfoDTO{" + "number=" + number + ", date=" + date + ", amount=" + amount + ", status=" + status + '}';
    }
}
