/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.persistence.repository;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import org.books.persistence.dto.OrderInfo;
import org.books.persistence.entity.Customer;
import org.books.persistence.entity.SalesOrder;

/**
 *
 * @author Adrian
 */
@Stateless
public class OrderRepository extends Repository<SalesOrder> {
    
    public OrderRepository() {
    } 
    
    public SalesOrder find(Long orderNr) {
        return find(SalesOrder.class, orderNr);
    }
    
    public List<OrderInfo> search(Customer customer, Integer year) {
        TypedQuery<OrderInfo> query = entityManager.createQuery(OrderInfo.GET_ALL_SALESORDER_BY_CUSTOMER_AND_YEAR, OrderInfo.class);
        query.setParameter("customerNumber", customer.getNumber());
        query.setParameter("from", getFrom(year));
        query.setParameter("to", getTo(year));

        return query.getResultList();
    }

    private Date getFrom(Integer year) {
        return getDate("01.01."+year);
    }
    private Date getTo(Integer year) {
        return getDate("31.12."+year);
    }
    private Date getDate(String date) {
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        try {
            return new Date(df.parse(date).getTime());
        } catch (ParseException ex) {
            throw new IllegalArgumentException(ex);
        }
    }
}
