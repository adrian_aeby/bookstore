/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.books.integration.amazon.generated;

/**
 *
 * @author baechlerf
 */
public class SecurityHelperDto {
    
    String associateTag;
    String awsAccessKeyId;
    String timeStamp;
    String signatur;
 
    public void setAssociateTag(String associateTag) {
        this.associateTag = associateTag;
    }

    public void setAwsAccessKeyId(String awsAccessKeyId) {
        this.awsAccessKeyId = awsAccessKeyId;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public void setSignatur(String signatur) {
        this.signatur = signatur;
    }
    
    public String getAssociateTag() {
        return associateTag;
    }

    public String getAwsAccessKeyId() {
        return awsAccessKeyId;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public String getSignatur() {
        return signatur;
    }
}
