package org.books.persistence.entity;

import java.math.BigDecimal;
import java.sql.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.books.persistence.entity.Address;
import org.books.persistence.entity.CreditCard;
import org.books.persistence.entity.Customer;
import org.books.persistence.entity.SalesOrderItem;
import org.books.persistence.enumeration.OrderStatus;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-01-27T20:55:24")
@StaticMetamodel(SalesOrder.class)
public class SalesOrder_ extends BaseEntity_ {

    public static volatile SingularAttribute<SalesOrder, Date> date;
    public static volatile SingularAttribute<SalesOrder, Long> number;
    public static volatile SingularAttribute<SalesOrder, BigDecimal> amount;
    public static volatile SingularAttribute<SalesOrder, Address> address;
    public static volatile SingularAttribute<SalesOrder, CreditCard> creditCard;
    public static volatile ListAttribute<SalesOrder, SalesOrderItem> items;
    public static volatile SingularAttribute<SalesOrder, OrderStatus> status;
    public static volatile SingularAttribute<SalesOrder, Customer> customer;

}