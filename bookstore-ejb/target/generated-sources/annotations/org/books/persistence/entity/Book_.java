package org.books.persistence.entity;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.books.persistence.enumeration.BookBinding;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-01-27T20:55:24")
@StaticMetamodel(Book.class)
public class Book_ extends BaseEntity_ {

    public static volatile SingularAttribute<Book, Integer> numberOfPages;
    public static volatile SingularAttribute<Book, BigDecimal> price;
    public static volatile SingularAttribute<Book, String> isbn;
    public static volatile SingularAttribute<Book, String> publisher;
    public static volatile SingularAttribute<Book, Integer> publicationYear;
    public static volatile SingularAttribute<Book, BookBinding> binding;
    public static volatile SingularAttribute<Book, String> title;
    public static volatile SingularAttribute<Book, String> authors;

}