package org.books.persistence.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.books.persistence.enumeration.UserGroup;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-01-27T20:55:24")
@StaticMetamodel(Login.class)
public class Login_ extends BaseEntity_ {

    public static volatile SingularAttribute<Login, String> password;
    public static volatile SingularAttribute<Login, String> userName;
    public static volatile SingularAttribute<Login, UserGroup> group;

}