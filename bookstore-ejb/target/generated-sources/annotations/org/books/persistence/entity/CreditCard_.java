package org.books.persistence.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.books.persistence.enumeration.CreditCardType;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-01-27T20:55:24")
@StaticMetamodel(CreditCard.class)
public class CreditCard_ { 

    public static volatile SingularAttribute<CreditCard, Integer> expirationYear;
    public static volatile SingularAttribute<CreditCard, String> number;
    public static volatile SingularAttribute<CreditCard, Integer> expirationMonth;
    public static volatile SingularAttribute<CreditCard, CreditCardType> type;

}