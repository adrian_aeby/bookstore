package org.books.persistence.entity;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.books.persistence.entity.Book;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-01-27T20:55:24")
@StaticMetamodel(SalesOrderItem.class)
public class SalesOrderItem_ extends BaseEntity_ {

    public static volatile SingularAttribute<SalesOrderItem, Integer> quantity;
    public static volatile SingularAttribute<SalesOrderItem, BigDecimal> price;
    public static volatile SingularAttribute<SalesOrderItem, Book> book;
    public static volatile SingularAttribute<SalesOrderItem, Long> id;

}